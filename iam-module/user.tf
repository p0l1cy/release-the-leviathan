resource "aws_iam_user" "user" {
  name = "user-${var.environment}"

  tags = {
    Environment = var.environment
  }
}

resource "aws_iam_user_group_membership" "group_membership" {
  user = aws_iam_user.user.name

  groups = [
    aws_iam_group.group.name,
  ]
}
